class Fixnum
  attr_accessor :number

  TWENTY_LESS = {
    0=>"zero",
    1=>"one",
    2=>"two",
    3=>"three",
    4=>"four",
    5=>"five",
    6=>"six",
    7=>"seven",
    8=>"eight",
    9=>"nine",
    10=>"ten",
    11=>"eleven",
    12=>"twelve",
    13 => "thirteen",
    14=>"fourteen",
    15=>"fifteen",
    16=>"sixteen",
    17=>"seventeen",
    18=>"eighteen",
    19=>"nineteen",
  }
  TWENTY_UP = {
    20=>"twenty",
    30=>"thirty",
    40=>"forty",
    50=>"fifty",
    60=>"sixty",
    70=>"seventy",
    80=>"eighty",
    90=>"ninety",
  }

  DEGREES_HIGH = {
    100=>"hundred",
    1000=>"thousand",
    1_000_000=>"million",
    1_000_000_000=>"billion",
    1_000_000_000_000=>"trillion"
}

  def initialize
    @number = self
  end

  def in_words
    if self < 100
      two_block_reader(self)
    elsif self < 1000
      three_block_reader(self)
    elsif self < 1_000_000
      read_thousands(self)
    else
      read_illions(self)
    end
  end

  def two_block_reader(int)
    read = ""
    if int < 20
      read = "#{TWENTY_LESS[self]}"
    elsif int < 100
      tens_place = "#{TWENTY_UP[(self / 10) * 10]}"
      ones_place = "#{TWENTY_LESS[self % 10]}"
      read = tens_place
      read = tens_place + " " + ones_place unless ones_place == "zero"
    end

    read
  end

  def three_block_reader(int)
    hundredths = int.to_s.chars[0]
    tens = int.to_s.chars[1..2].join('')
    tens = in_words(tens.to_i)

    read = "#{TWENTY_LESS[hundredths.to_i]} hundred" if tens == 0
    read = "#{TWENTY_LESS[hundredths.to_i]} hundred #{tens}"
  end

  def read_thousands(int)
    read = "#{TWENTY_LESS[hundredths.to_i]} thousand #{in_words(three_digit_taker)}"
  end

  def read_illions
  end

  def three_digit_taker(int)
    three_digit_taker = int.to_s.chars.pop[-3..-1]
    three_digit_taker = three_digit_taker.join('').to_i
    three_digit_taker = in_words(three_digit_taker)
  end

end
